<?php

/**
 * @file
 * Admin page callbacks for the Spectre module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * File system settings form builder.
 */
function spectre_admin_filesystem_settings() {
  $form = array();

  $form['spectre_root_cache_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Root cache directory'),
    '#default_value' => variable_get('spectre_root_cache_dir', SPECTRE_ROOT_CACHE_DIR),
  );

  $form['spectre_normal_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Normal cache directory'),
    '#default_value' => variable_get('spectre_normal_dir', SPECTRE_NORMAL_DIR),
  );

  $form['spectre_char'] = array(
    '#type' => 'textfield',
    '#title' => t('Character replacement for "?" in the URL'),
    '#default_value' => variable_get('spectre_char', SPECTRE_CHAR),
  );

  // Reset .htaccess on submit.
  $form['#submit'][] = 'spectre_form_submit_handler';

  return system_settings_form($form);
}
