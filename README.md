# spectre

Serves JavaScript-rendered pages as HTML with Prerender.io

## To do

* Add option to render URLs in sitemap only (to avoid hitting prerender.io limit)
* Should set `_escaped_fragment_` if it is not set in query string, so that there is only 1 cache file per URL. Example: filename for `/` with fragment is `__escaped_fragment_=%2F.html` and without is `_.html`

## Introduction

* https://developers.google.com/webmasters/ajax-crawling
* https://github.com/prerender/prerender

## Credits

* [Boost](http://drupal.org/project/boost)
* [Laravel Prerender](https://github.com/JeroenNoten/Laravel-Prerender)
