<?php

/**
 * @file
 * Admin page callbacks for the Spectre module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Render settings form builder.
 */
function spectre_admin_render_settings() {
  $form = array();

//  $url = spectre_parse_url();
//  $info = spectre_admin_render_get_url_info($url['url']);
//  dpm($url);
//  dpm($info);

  $form['render'] = array(
    '#type' => 'submit',
    '#value' => t('Render URLs in sitemap'),
    '#submit' => array('spectre_admin_render_render_submit'),
  );

  return $form;
}

/**
 *
 * @global string $base_root
 * @global string $base_path
 * @param string $url
 * @return array
 * @see spectre_get_url_info()
 */
function spectre_admin_render_get_url_info($url) {
  global $base_root, $base_path;

  $parts = spectre_parse_url($url, $base_path);

  if (!$parts) {
    return array('is_renderable' => FALSE);
  }

  $parts['base_dir'] = spectre_get_normal_cache_dir() . '/' . $parts['host'] . $base_path;
  $parts['filename'] = $parts['base_dir'] . $parts['full_path'] . variable_get('spectre_char', SPECTRE_CHAR) . $parts['query'];
  $parts['directory'] = dirname($parts['filename']);

  $front_page = variable_get('site_frontpage', 'node');

  // Get the internal path (node/8).
  if ($parts['path'] === '' || $parts['path'] == $front_page) {
    $parts['normal_path'] = variable_get('site_frontpage', 'node');
  }
  else {
    $parts['normal_path'] = drupal_get_normal_path($parts['path']);
  }

  // Get the alias (content/about-us).
  $parts['path_alias'] = drupal_get_path_alias($parts['normal_path']);

  // Get all args.
  $args = arg(NULL, $parts['normal_path']);

  // Prevent array warnings.
  $args[0] = empty($args[0]) ? '' : $args[0];
  $args[1] = empty($args[1]) ? '' : $args[1];
  $args[2] = empty($args[2]) ? '' : $args[2];
  $parts['args'] = $args;

  // Get content type.
  if (!empty($parts['normal_path'])) {
    $parts = _spectre_get_menu_router($parts);
  }

  // Check if the URL is cacheable.
  $parts['is_renderable'] = spectre_admin_render_is_renderable($parts);

  return $parts;
}

/**
 *
 * @param array $parts
 * @return boolean
 * @see spectre_is_renderable
 */
function spectre_admin_render_is_renderable($parts) {
  if (empty($parts['menu_item'])
      || $parts['menu_item']['status'] != 200
      || (!spectre_is_path_cacheable($parts['normal_path']) && !spectre_is_path_cacheable($parts['path_alias']))
  ) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Form submit handler.
 *
 * @see spectre_admin_render_settings()
 */
function spectre_admin_render_render_submit() {
  global $base_url;

  $sitemap_base_url = variable_get('xmlsitemap_base_url', $base_url);
  $url = $base_url . '/sitemap.xml';
  $response = _spectre_admin_render_http_request($url);

  if (empty($response)) {
    drupal_set_message(t('Could not load !url (no response)', array('!url' => $url)), 'error');
  }
  elseif (!empty($response->error)) {
    drupal_set_message(t('An error occurred while trying to load !url: !error', array('!url' => $url, '!error' => $response->error)), 'error');
  }
  elseif (empty($response->data)) {
    drupal_set_message(t('Could not load !url (no data)', array('!url' => $url)), 'error');
  }
  else {
    $data = new SimpleXMLElement($response->data);
    $data = drupal_json_decode(drupal_json_encode($data));
    $urls = array();

    if (!empty($data['url'])) {
      foreach ($data['url'] as $item) {
        $url = $item['loc'];

        if (!empty($url) && strpos($url, $sitemap_base_url) === 0) {
          $info = spectre_admin_render_get_url_info($url);

          if ($info['is_renderable']) {
            $parse = drupal_parse_url($url);
            $parts = parse_url($parse['path']);
            $query = array('_escaped_fragment_' => isset($parts['path']) ? $parts['path'] : '/');
            $urls[] = rtrim($sitemap_base_url, '/') . '/?' . http_build_query($query);
          }
        }
      }
    }

    if (empty($urls)) {
      drupal_set_message(t('There are no URLs in the sitemap to process.'));
    }
    else {
      $operations = array();

//      $urls = array($urls[0], $urls[1]);

      foreach ($urls as $url) {
        $operations[] = array('spectre_admin_render_render_batch', array($url));
      }

      $batch = array(
        'title' => 'Rendering URLs',
        'operations' => $operations,
        'finished' => 'spectre_admin_render_render_batch_finished',
        'file' => drupal_get_path('module', 'spectre') . '/spectre.admin.render.inc',
      );
      batch_set($batch);
    }
  }
}

/**
 * Batch operation.
 *
 * @param array $params
 * @param array $context
 * @see spectre_admin_render_render_submit()
 */
function spectre_admin_render_render_batch($params, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = 1;
  }

  $response = _spectre_admin_render_http_request($params);
  $result = !empty($response) && empty($response->error) && !empty($response->data);

  $context['results'][] = array($params, $result);
  $context['sandbox']['progress'] ++;

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch finished callback.
 *
 * @see spectre_admin_render_render_submit()
 */
function spectre_admin_render_render_batch_finished($success, $results, $operations) {
  $type = 'status';

  if ($success) {
    $hits = 0;
    $misses = 0;

    foreach ($results as $result) {
      if (empty($result[1])) {
        $misses++;
      }
      else {
        $hits++;
      }
    }

    // If only hits.
    if (!$misses) {
      $message = format_plural(count($results), '@count URL rendered.', '@count URLs rendered.');
    }
    // If only misses.
    elseif (!$hits) {
      $message = format_plural(count($results), '@count URL failed to render.', '@count URLs failed to render.');
      $type = 'error';
    }
    // If hits and misses.
    else {
      $message = format_plural($hits, '@count URL rendered.', '@count URLs rendered.');
      $message .= ' ';
      $message .= format_plural($misses, '@count URL failed to render.', '@count URLs failed to render.');
      $type = 'error';
    }
  }
  else {
    $error_operation = reset($operations);
    $message = 'An error occurred while creating ' . $error_operation[0] . ' with arguments :' . print_r($error_operation[0], TRUE);
    $type = 'error';
  }

  drupal_set_message($message, $type);
}

/**
 * Performs an HTTP request. Similar to drupal_http_request() but uses cURL
 * instead of socket connections, if possible.
 *
 * @param string $url
 * @param array $options
 * @return stdClass
 */
function _spectre_admin_render_http_request($url, $options = array()) {
  if (function_exists('curl_exec')) {
    // Merge the default options.
    $options += array(
      'headers' => array(),
      'method' => 'GET',
      'data' => NULL,
      'max_redirects' => 3,
      'timeout' => 30.0,
      'context' => NULL,
    );

    // Merge the default headers.
    $options['headers'] += array(
      'User-Agent' => 'Drupal (+http://drupal.org/)',
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $options['headers']);
    curl_setopt($ch, CURLOPT_USERAGENT, $options['headers']['User-Agent']);
    curl_setopt($ch, CURLOPT_MAXREDIRS, $options['max_redirects']);
    curl_setopt($ch, CURLOPT_TIMEOUT, $options['timeout']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    $data = curl_exec($ch);
    curl_close($ch);

    return (object) array(
          'error' => FALSE,
          'data' => $data,
    );
  }

  return drupal_http_request($url, $options);
}
