<?php

/**
 * @file
 * Admin page callbacks for the Spectre module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Debug settings form builder.
 */
function spectre_admin_debug_settings() {
  $form = array();

  $form['spectre_message_debug'] = array(
    '#title' => t('Send debug info for each request to watchdog.'),
    '#description' => t('Only use for debugging purposes as this can fill up watchdog fairly quickly.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('spectre_message_debug', SPECTRE_MESSAGE_DEBUG),
  );

  // Reset .htaccess on submit.
  $form['#submit'][] = 'spectre_form_submit_handler';

  return system_settings_form($form);
}
