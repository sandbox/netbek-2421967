<?php

/**
 * @file
 * Installation and upgrade tasks for the Spectre module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Implements hook_enable().
 */
function spectre_enable() {
  spectre_htaccess_cache_dir_put();
}

/**
 * Implements hook_disable().
 */
function spectre_disable() {
  // Make sure that the Spectre cache is wiped when the module is disabled.
  spectre_flush_caches();
  drupal_set_message(t('Spectre cache cleared.'));
}

/**
 * Implements hook_install()
 */
function spectre_install() {
  db_query("UPDATE {system} SET weight = 1 WHERE name = 'spectre'");
}

/**
 * Implements hook_uninstall()
 */
function spectre_uninstall() {
  $result = db_select('variable', 'v')
      ->fields('v', array('name'))
      ->condition('name', 'spectre_%', 'LIKE')
      ->execute();
  foreach ($result as $row) {
    variable_del($row->name);
  }
}

/**
 * Implements hook_requirements().
 */
function spectre_requirements($phase) {
  $requirements = array();
  $t = get_t();

  // Check the server's ability to use Spectre
  if ($phase == 'runtime') {
    // Check cache directories
    $cache_directories = array(
      DRUPAL_ROOT . '/' . spectre_get_normal_cache_dir(),
    );
    foreach ($cache_directories as $cache_directory) {
      spectre_mkdir($cache_directory);

      if (!is_dir($cache_directory)) {
        $requirements['spectre_default'] = array(
          'title' => $t('Spectre'),
          'description' => $t('!cache_dir: does not exist.', array('!cache_dir' => $cache_directory)),
          'severity' => REQUIREMENT_ERROR,
          'value' => $t('Cache path'),
        );
      }
      else if (!is_writable($cache_directory)) {
        $requirements['spectre_permissions'] = array(
          'title' => $t('Spectre'),
          'description' => $t('Directory %dir credentials - Permissions: %fp. Owner %fo. Group %fg.<br /> Your credentials - Group ID: %gid. User ID: %uid. Current script owner: %user.', array('%dir' => getcwd() . '/' . $cache_directory, '%gid' => getmygid(), '%uid' => getmyuid(), '%user' => get_current_user(), '%fp' => substr(sprintf('%o', fileperms($cache_directory)), -4), '%fo' => fileowner($cache_directory), '%fg' => filegroup($cache_directory))),
          'severity' => REQUIREMENT_ERROR,
          'value' => $t('Cannot write to file system'),
        );
      }
    }

    $enable = variable_get('spectre_enable', 0);

    // Check if the Prerender URL is set.
    $prerender_url = variable_get('spectre_prerender_url', '');
    if (!empty($enable) && empty($prerender_url)) {
      $requirements['spectre_prerender_url'] = array(
        'title' => $t('Spectre'),
        'severity' => REQUIREMENT_ERROR,
        'value' => $t('Prerender URL is not set (<a href="@settings">configure</a>).', array('@settings' => url('admin/config/system/spectre'))),
      );
    }

    if (empty($requirements)) {
      $requirements['spectre'] = array(
        'title' => $t('Spectre'),
        'severity' => REQUIREMENT_OK,
        'value' => empty($enable) ? $t('Not enabled') : $t('Spectre installed correctly, should be working if properly <a href="@settings">configured</a>.', array('@settings' => url('admin/config/system/spectre'))),
      );
    }
  }
  return $requirements;
}
