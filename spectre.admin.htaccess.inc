<?php

/**
 * @file
 * Admin page callbacks for the Spectre module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */
/**
 * Default Apache server name.
 */
define('SPECTRE_SERVER_NAME_HTTP_HOST', '%{HTTP_HOST}');

/**
 * Default Apache document root.
 */
define('SPECTRE_DOCUMENT_ROOT', '%{DOCUMENT_ROOT}');

/**
 * Default setting for SSL pages
 */
define('SPECTRE_SSL_BYPASS', TRUE);

/**
 * .htaccess settings form builder.
 */
function spectre_admin_htaccess_settings() {
  global $base_path;

  $form = array();

  $form['htaccess'] = array(
    '#type' => 'fieldset',
    '#title' => t('Spectre Apache .htaccess settings'),
    '#description' => t('<a href="!link">Explanation of .htaccess variables</a> <br /><br /> <strong>Be sure to save the configuration and then go to the <a href="!rules">.htaccess rules generation page</a> and copy the rules.</strong>', array('!link' => url('http://www.askapache.com/htaccess/mod_rewrite-variables-cheatsheet.html'), '!rules' => url('admin/config/system/spectre/htaccess/generator'))),
  );

  $form['htaccess']['spectre_server_name_http_host'] = array(
    '#type' => 'radios',
    '#title' => t("Server's URL or name"),
    '#default_value' => variable_get('spectre_server_name_http_host', SPECTRE_SERVER_NAME_HTTP_HOST),
    '#options' => array(
      '%{HTTP_HOST}' => '%{HTTP_HOST}',
      '%{SERVER_NAME}' => '%{SERVER_NAME}',
      $_SERVER['HTTP_HOST'] => $_SERVER['HTTP_HOST'],
      $_SERVER['SERVER_NAME'] => $_SERVER['SERVER_NAME'],
    ),
    '#description' => t('Best to leave these as %{}, only try the last option(s) if Spectre is still not working.'),
  );

  // Set DOCUMENT_ROOT
  $drupal_subdir = rtrim($base_path, '/');
  $document_root = str_replace("\\", '/', getcwd()); // fix windows dir slashes
  $document_root = trim(str_replace($drupal_subdir, '', $document_root)); // remove subdir
  $options = array('%{DOCUMENT_ROOT}' => '%{DOCUMENT_ROOT}', $document_root => $document_root); // initial options
  $rejects = array('SCRIPT_FILENAME', 'DOCUMENT_ROOT'); // values to ignore
  $output = spectre_admin_htaccess_array_find($document_root, $_SERVER, $rejects); //search for values that match getcwd
  $description_extra = '';
  if (!empty($output)) {
    foreach ($output as $key => $value) {
      $temp = '%{ENV:' . $key . '}';
      $options[$temp] = $temp . ' = ' . $value; // adding values to options
      if (strcmp($value, $document_root) == 0) {
        $best = $temp; // set best since it's a match
      }
    }
  }
  if (strcmp($_SERVER['DOCUMENT_ROOT'], $document_root) == 0) {
    $best = '%{DOCUMENT_ROOT}';
  }
  elseif (!isset($best)) {
    $best = $document_root;
//    $description_extra = t('Please <a href="!link">open an spectre issue on Drupal.org</a>, since apache and php might not be configured correctly.', array('!link' => url('http://drupal.org/node/add/project-issue/spectre')));
  }
  $percent = 0;
  $int = similar_text(substr(trim($_SERVER['DOCUMENT_ROOT']), 18, 1), substr(trim($document_root), 18, 1), $percent);
  $description = t('Value of %best is recommended for this server.', array('%best' => $best)) . ' ' . $description_extra;
  $form['htaccess']['spectre_document_root'] = array(
    '#type' => 'radios',
    '#title' => t('Document root'),
    '#default_value' => variable_get('spectre_document_root', SPECTRE_DOCUMENT_ROOT),
    '#options' => $options,
    '#description' => $description,
  );

  $form['htaccess']['spectre_apache_etag'] = array(
    '#type' => 'radios',
    '#title' => t('ETag settings'),
    '#default_value' => variable_get('spectre_apache_etag', SPECTRE_APACHE_ETAG),
    '#options' => array(
      3 => "Set FileETag 'MTime Size' - Useful in server clusters (Highly Recommended)",
      2 => "Set FileETag 'All' - Default if enabled",
      1 => "Set FileETag 'None' - Do not send an etag",
      0 => 'Do nothing',
    ),
    '#description' => t('Uses <a href="!link">FileETag Directive</a> to set <a href="!about">ETags</a> for the files cached by Spectre. <a href="!stack">More info on this subject</a>', array('!link' => url('http://httpd.apache.org/docs/trunk/mod/core.html#fileetag'), '!about' => url('http://en.wikipedia.org/wiki/HTTP_ETag'), '!stack' => url('http://stackoverflow.com/questions/tagged?tagnames=etag&sort=votes&pagesize=50'))),
  );

  $form['htaccess']['spectre_apache_xheader'] = array(
    '#type' => 'radios',
    '#title' => t('Spectre tags'),
    '#default_value' => variable_get('spectre_apache_xheader', SPECTRE_APACHE_XHEADER),
    '#options' => array(
      1 => 'Set Spectre header',
      0 => 'Do not set Spectre header',
    ),
    '#description' => t('In order to identify that the page is being served from the cache, Spectre can send out a header that will identify any files served from the Spectre cache.'),
  );

  $form['htaccess']['spectre_ssl_bypass'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bypass the Spectre cache for SSL requests.'),
    '#default_value' => variable_get('spectre_ssl_bypass', SPECTRE_SSL_BYPASS),
    '#description' => t('Ticking this is recommended if you use the securepages module.'),
  );

  $form['htaccess']['spectre_add_default_charset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add "AddDefaultCharset X" to the .htaccess rules'),
    '#default_value' => variable_get('spectre_add_default_charset', SPECTRE_ADD_DEFAULT_CHARSET),
    '#description' => t('Depending on your i18n settings you might want this disabled or enabled. X is set below'),
  );

  $form['htaccess']['spectre_charset_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Add "AddDefaultCharset utf-8" to the .htaccess rules'),
    '#default_value' => variable_get('spectre_charset_type', SPECTRE_CHARSET_TYPE),
    '#description' => t('Depending on your i18n settings you might want this disabled or enabled.'),
  );

  $form['htaccess']['spectre_match_symlinks_options'] = array(
    '#type' => 'radios',
    '#title' => t('%cache_folder Options', array('%cache_folder' => $document_root . '/' . variable_get('spectre_root_cache_dir', SPECTRE_ROOT_CACHE_DIR) . '/' . variable_get('spectre_normal_dir', SPECTRE_NORMAL_DIR) . '/' . variable_get('spectre_server_name_http_host', SPECTRE_SERVER_NAME_HTTP_HOST) . '/.htaccess')),
    '#default_value' => variable_get('spectre_match_symlinks_options', SPECTRE_MATCH_SYMLINKS_OPTIONS),
    '#options' => array(
      1 => 'Set "Options +FollowSymLinks"',
      0 => 'Set "Options +SymLinksIfOwnerMatch"',
    ),
    '#description' => t('The .htaccess file in the cache folder requires "Options +FollowSymLinks" or "Options +SymLinksIfOwnerMatch" for mod_rewrite. Some hosting companies only permit the SymLinksIfOwnerMatch option. If you get a http 500 error code try setting SymLinksIfOwnerMatch.'),
  );

  // Reset .htaccess on submit.
  $form['#submit'][] = 'spectre_form_submit_handler';

  return system_settings_form($form);
}

/**
 * .htaccess file form builder.
 */
function spectre_admin_htaccess_generation() {
  $form = array();

  $htaccess = spectre_admin_htaccess_generate_htaccess();
  $form['spectre_htaccess'] = array(
    '#type' => 'textarea',
    '#title' => t('Generated rules'),
    '#description' => t('Copy this into your .htaccess file.'),
    '#readonly' => TRUE,
    '#wysiwyg' => FALSE,
    '#rows' => count(explode("\n", $htaccess)) + 1,
    '#default_value' => $htaccess,
  );

  // Reset .htaccess on submit.
  $form['#submit'][] = 'spectre_form_submit_handler';

  return $form;
}

/**
 *
 * @return string
 */
function spectre_admin_htaccess_generate_htaccess() {
  global $base_path;

  $user_agents = variable_get('spectre_user_agents', '');
  $server_name = variable_get('spectre_server_name_http_host', SPECTRE_SERVER_NAME_HTTP_HOST);
  $document_root = variable_get('spectre_document_root', SPECTRE_DOCUMENT_ROOT);
  $drupal_subdir = rtrim($base_path, '/');
  $cache_dir = variable_get('spectre_root_cache_dir', SPECTRE_ROOT_CACHE_DIR);
  $normal_dir = variable_get('spectre_normal_dir', SPECTRE_NORMAL_DIR);
  $char = variable_get('spectre_char', SPECTRE_CHAR);

  // Go through every storage type getting the extesion and if it supports gzip.
  $enabled_file_extensions = array();
  $types = spectre_get_storage_types();
  foreach ($types as $title => $content_types) {
    foreach ($content_types as $type => $values) {
      if ($values['enabled']) {
        $enabled_file_extensions[$values['extension']]['gzip'] = $values['gzip'];
        if (empty($enabled_file_extensions[$values['extension']]['content_type'])) {
          $enabled_file_extensions[$values['extension']]['content_type'] = $type;
        }
      }
    }
  }

  $output = array('gzip' => '', 'normal' => '');
  $gzip_count = 0;
  $normal_count = 0;
  foreach ($enabled_file_extensions as $extension => $values) {
    $type = $values['content_type'];
    if ($values['gzip']) {
      $output['gzip'] .= "RewriteCond $document_root$base_path$cache_dir/%{ENV:spectrepath}/$server_name%{REQUEST_URI}$char%{QUERY_STRING}\.$extension\.gz -s\n";
      $output['gzip'] .= "RewriteRule .* $cache_dir/%{ENV:spectrepath}/$server_name%{REQUEST_URI}$char%{QUERY_STRING}\.$extension\.gz [L,T=$type,E=no-gzip:1]\n";
      $gzip_count++;
    }
    $output['normal'] .= "RewriteCond $document_root$base_path$cache_dir/%{ENV:spectrepath}/$server_name%{REQUEST_URI}$char%{QUERY_STRING}\.$extension -s\n";
    $output['normal'] .= "RewriteRule .* $cache_dir/%{ENV:spectrepath}/$server_name%{REQUEST_URI}$char%{QUERY_STRING}\.$extension [L,T=$type]\n";
    $normal_count++;
  }
  $skip = !empty($gzip_count) ? $normal_count + $gzip_count + 1 : $normal_count;

  // Skip if request URI starts with.
  $start = array(
    $cache_dir,
    'admin',
    'authenticate',
    'batch',
    'comment/reply',
    'devel',
    'file/add',
    'includes',
    'misc',
    'modules',
    'node/add',
    'openid',
    'profiles',
    'scripts',
    'sites',
    'themes',
    'user/(autocomplete|login|logout|password|register|reset)',
  );

  // Skip if request URI ends with.
  $end = array(
    'cancel',
    'delete',
    'devel',
    'edit',
    'revert',
    'user',
  );

  $string = "### SPECTRE START ###\n";

  if (!empty($output)) {
    $string .= "\n";
    $string .= "# Allow for alt paths to be set via .htaccess rules; allows for cached variants (future mobile support)\n";
    $string .= "RewriteRule .* - [E=spectrepath:$normal_dir]\n";
    $string .= "\n";
    $string .= "# Skip spectre IF user agent does not match AND _escaped_fragment_ not in query string\n";
    $string .= "RewriteCond %{HTTP_USER_AGENT} !(" . str_replace(array("\n", " "), array("|", "\ "), preg_quote($user_agents)) . ") [NC]\n";
    $string .= "RewriteCond %{QUERY_STRING} !(^|&)_escaped_fragment_($|&|=) [NC]\n";
    $string .= "RewriteRule .* - [S=" . ($skip + 1) . "]\n";
    $string .= "\n";
    $string .= "# Skip spectre IF not get request OR uri has wrong dir OR cookie is set OR request came from this server" . (variable_get('spectre_ssl_bypass', SPECTRE_SSL_BYPASS) ? " OR https request" : "") . "\n";
    $string .= "RewriteCond %{REQUEST_METHOD} !^(GET|HEAD)$ [OR]\n";
    $string .= "RewriteCond %{REQUEST_URI} (^$base_path(" . implode("|", $start) . "))|(/(" . implode("|", $end) . ")$) [OR]\n";
    if (variable_get('spectre_ssl_bypass', SPECTRE_SSL_BYPASS)) {
      $string .= "RewriteCond %{HTTPS} on [OR]\n";
    }
    $string .= "RewriteCond %{HTTP_COOKIE} " . variable_get('spectre_cookie', SPECTRE_COOKIE) . " [OR]\n";
    $string .= "RewriteCond %{ENV:REDIRECT_STATUS} 200\n";
    $string .= "RewriteRule .* - [S=$skip]\n";
    $string .= "\n";
    $string .= "# GZIP\n";
    $string .= "RewriteCond %{HTTP:Accept-encoding} !gzip\n";
    $string .= "RewriteRule .* - [S=$gzip_count]\n";
    $string .= $output['gzip'];
    $string .= "\n";
    $string .= "# NORMAL\n";
    $string .= $output['normal'];
  }

  $string .= "\n";
  $string .= "### SPECTRE END ###\n";

  return $string;
}

/**
 * Returns all key/values in array that are equal.
 *
 * @param string $needle What you are searching for
 * @param array $haystack Array of values
 * @param array $a_not Optional array of key names to exclude
 */
function spectre_admin_htaccess_array_find($needle, $haystack, $a_not = array()) {
  $out = array();
  foreach ($haystack as $key => $value) {
    if (is_string($value) && strstr($value, $needle) !== FALSE) {
      $good = TRUE;
      foreach ($a_not as $not) {
        if (strpos($key, $not) !== FALSE) {
          $good = FALSE;
        }
      }
      if ($good) {
        $out[$key] = $value;
      }
    }
  }
  return $out;
}
