<?php

/**
 * @file
 * Admin page callbacks for the Spectre module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Settings form builder.
 */
function spectre_admin_settings() {
  $form = array();

  $form['spectre_enable'] = array(
    '#title' => t('Enabled'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('spectre_enable', FALSE),
  );

  $form['prerender'] = array(
    '#title' => t('Prerender'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );

  $form['prerender']['spectre_prerender_url'] = array(
    '#title' => t('Endpoint URL'),
    '#description' => t('URL of the prerender server. By default, Prerender\'s hosted service is used (<code>https://service.prerender.io</code>). But you can also set it to your own server address.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('spectre_prerender_url', SPECTRE_PRERENDER_URL),
  );

  $form['prerender']['spectre_prerender_token'] = array(
    '#title' => t('Token'),
    '#description' => t('If you use prerender.io as service, you need to set your prerender.io token here. It will be sent via the <code>X-Prerender-Token</code> header. If you do not provide a token, the header will not be added.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('spectre_prerender_token', SPECTRE_PRERENDER_TOKEN),
  );

  $form['selection'] = array(
    '#title' => t('Selection'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );

  $form['selection']['spectre_whitelist'] = array(
    '#type' => 'textarea',
    '#title' => t('Whitelist'),
    '#description' => t('A list of Drupal paths that should be prerendered, one per line.'),
    '#default_value' => variable_get('spectre_whitelist', ''),
    '#rows' => 10,
  );

  $form['selection']['spectre_blacklist'] = array(
    '#type' => 'textarea',
    '#title' => t('Blacklist'),
    '#description' => t('A list of Drupal paths that should not be prerendered, one per line.'),
    '#default_value' => variable_get('spectre_blacklist', implode("\n", spectre_mandatory_blacklist())),
    '#rows' => 10,
  );

  $default_user_agents = spectre_user_agents();
  $user_agents = variable_get('spectre_user_agents', implode("\n", $default_user_agents['enable']));

  $form['selection']['spectre_user_agents'] = array(
    '#type' => 'textarea',
    '#title' => t('User agents that do not support <code>_escaped_fragment_</code>'),
    '#description' => t('One per line. Search engine crawlers that can crawl URLs with a hash fragment are shown the prerendered page only if their request includes the <code>_escaped_fragment_</code> query parameter.'),
    '#default_value' => $user_agents,
    '#rows' => 10,
  );

  $types = spectre_get_storage_types();
  $period = drupal_map_assoc(array(0, 60, 180, 300, 600, 900, 1800, 2700, 3600, 10800, 21600, 32400, 43200, 64800, 86400, 2 * 86400, 3 * 86400, 4 * 86400, 5 * 86400, 6 * 86400, 604800, 2 * 604800, 3 * 604800, 4 * 604800, 8 * 604800, 16 * 604800, 52 * 604800), 'format_interval');

  $form['cache_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cache type settings'),
  );
  foreach ($types as $title => $content_types) {
    $form['cache_types'][$title] = array(
      '#type' => 'fieldset',
      '#title' => t('@title settings', array('@title' => $title)),
      '#collapsible' => TRUE,
    );
    $collapsed = TRUE;
    foreach ($content_types as $type => $values) {
      $form['cache_types'][$title][$type] = array(
        '#type' => 'fieldset',
        '#title' => t('@type settings', array('@type' => $type)),
        '#description' => t('Cache @description of type @type', array(
          '@description' => $values['description'],
          '@type' => $type,
            )
        ),
      );
      // This content type enabled?
      $form['cache_types'][$title][$type]['spectre_enabled_' . $type] = array(
        '#type' => 'checkbox',
        '#title' => t('Cache enabled'),
        '#default_value' => $values['enabled'],
      );

      // Enable gzip?
      $form['cache_types'][$title][$type]['spectre_gzip_' . $type] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable gzip compression'),
        '#description' => (SPECTRE_GZIP ? t('Avoids having to compress the content by the web server on every request (recommended).') : t('Your host does not support zlib. See: !url', array('!url' => 'http://www.php.net/manual/en/zlib.installation.php'))),
        '#default_value' => (SPECTRE_GZIP ? $values['gzip'] : 0),
        '#disabled' => !SPECTRE_GZIP,
      );

      // Content type extension
      $form['cache_types'][$title][$type]['spectre_extension_' . $type] = array(
        '#type' => 'textfield',
        '#title' => t('Filename extension', array(
          '@title' => $title,
          '@description' => $values['description'],
          '@type' => $type,
            )
        ),
        '#default_value' => $values['extension'],
      );

      // Maximum cache lifetime
      $form['cache_types'][$title][$type]['spectre_lifetime_max_' . $type] = array(
        '#type' => 'select',
        '#options' => $period,
        '#title' => t('@type - Maximum cache lifetime', array(
          '@title' => $title,
          '@description' => $values['description'],
          '@type' => $type,
            )
        ),
        '#default_value' => $values['lifetime_max'],
      );

      // Minimum cache lifetime
      $form['cache_types'][$title][$type]['spectre_lifetime_min_' . $type] = array(
        '#type' => 'select',
        '#options' => $period,
        '#title' => t('@type - Minimum cache lifetime', array(
          '@title' => $title,
          '@description' => $values['description'],
          '@type' => $type,
            )
        ),
        '#default_value' => $values['lifetime_min'],
      );
      if ($values['enabled']) {
        $collapsed = !$values['enabled'];
      }
    }
    $form['cache_types'][$title]['#collapsed'] = $collapsed;
  }

  // Reset .htaccess on submit.
  $form['#submit'][] = 'spectre_form_submit_handler';

  return system_settings_form($form);
}

/**
 * Validation callback for the settings form.
 */
function spectre_admin_settings_validate($form, &$form_state) {
  // Normalize newline character and remove empty lines
  $keys = array(
    'spectre_whitelist',
    'spectre_blacklist',
    'spectre_user_agents',
  );
  foreach ($keys as $key) {
    $value = $form_state['values'][$key];
    $value = str_replace(array("\r\n", "\r"), "\n", trim($value));
    $value = preg_replace('/^\s*/m', '', $value);
    $form_state['values'][$key] = $value;
  }

  $blacklist = array();

  // Add mandatory blacklist paths.
  if ($form_state['values']['spectre_blacklist'] === '') {
    $blacklist = spectre_mandatory_blacklist();
  }
  else {
    $blacklist = explode("\n", $form_state['values']['spectre_blacklist']);
    $blacklist = array_merge(spectre_mandatory_blacklist(), $blacklist);
    $blacklist = array_unique($blacklist);
    sort($blacklist);
  }
  $form_state['values']['spectre_blacklist'] = implode("\n", $blacklist);

  // Remove blacklist paths from whitelist.
  if ($form_state['values']['spectre_whitelist'] !== '') {
    $whitelist = explode("\n", $form_state['values']['spectre_whitelist']);
    $whitelist = array_diff($whitelist, $blacklist);
    $whitelist = array_unique($whitelist);
    sort($whitelist);
    $form_state['values']['spectre_whitelist'] = implode("\n", $whitelist);
  }
}
