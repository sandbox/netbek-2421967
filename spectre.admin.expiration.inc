<?php

/**
 * @file
 * Admin page callbacks for the Spectre module.
 *
 * @author Hein Bekker <hein@netbek.co.za>
 * @copyright (c) 2015 Hein Bekker
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GPLv2
 */

/**
 * Expiration settings form builder.
 */
function spectre_admin_expiration_settings() {
  $form = array();

  $form['spectre_ignore_flush'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ignore a cache flush command if cron issued the request.'),
    '#default_value' => variable_get('spectre_ignore_flush', SPECTRE_IGNORE_FLUSH),
    '#description' => t('Drupal will flush all caches when cron is executed, depending on the <a href="!urlcore">core minimum cache lifetime</a> setting. To ignore the request to flush the cache on cron runs, enable this option. If enabled, pages in the Spectre cache will be flushed only when their <a href="!urlspectre">Spectre maximum cache lifetime</a> expires.', array(
      '!urlcore' => url('admin/config/development/performance'),
      '!urlspectre' => url('admin/config/system/spectre'),
    )),
  );

  $form['spectre_expire_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove old cache files on cron.'),
    '#default_value' => variable_get('spectre_expire_cron', SPECTRE_EXPIRE_CRON),
    '#description' => t('If enabled, each time cron runs, Spectre will check each cached page and delete those that have expired (maximum cache lifetime). The expiration time is displayed in the comment that Spectre adds to the bottom of the html pages it creates. This setting is recommended for most sites.'),
  );

  // Reset .htaccess on submit.
  $form['#submit'][] = 'spectre_form_submit_handler';

  return system_settings_form($form);
}
